//
//  HomeViewModel.swift
//  mediaWeather
//
//  Created by Pavel Belov on 20.10.2021.
//

import Foundation
import UIKit

class HomeViewModel {
    
    var current: CurrentData?
    var oneCall: OneCall?
    var onUpdate: (()->Void)?
    var onDetail: ((_: Double, _: Double)->Void)?
    var initial_name: String?
    var transferCoords: ((_: Double, _: Double)->Void)?
    
   
    func retrive() {
        DispatchQueue.global().async {
            if let arg = self.initial_name {
                let queries = UrlQueries.current(arg).fill()
                let session = SessionManager([:], .post, queries, .current)
                session.downloadData { [weak self] result in
                    switch result {
                    case .success(let data):
                        if let reply = try? JSONDecoder().decode(CurrentData.self, from: data) {
                            DispatchQueue.main.async {
                                self?.current = reply
                                if let coor = reply.coord, let lat = coor.lat, let lon = coor.lon {
                                    self?.transferCoords!(lon, lat)
                                }
                                
                                self?.onUpdate?()
                            }
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
    
}
