//
//  Current.swift
//  mediaWeather
//
//  Created by Pavel Belov on 13.10.2021.
//

import Foundation

class CurrentData: Decodable {
    
    var name: String?
    var main: Main?
    var coord: Coord?
    var weather: [Weather]?
    var wind: Wind?
    var visibility: Int?
    var sys: Sys?
 
}

class Coord: Decodable {
    var lon: Double?
    var lat: Double?
}

class Main: Decodable {
    var temp: Double?
    var feels_like: Double?
    var temp_min: Double?
    var pressure: Int?
    var humidity: Int?
    var temp_max: Double?
}


class  Wind: Decodable {
    var deg: Int?
    var speed: Double?
    var gust: Double?
    
    var wind_segment: String {
        
        var seg: String = ""
        
        if let dir = self.deg {
            switch dir {
            case 0..<90 :
                seg = "С-В"
            case 91..<180 :
                seg = "Ю-В"
            case 181..<270 :
                seg = "Ю-З"
            case 271..<360 :
                seg = "С-З"
            default:
                seg = ""
            }
        }
       return seg
    }
}

class Sys: Decodable {
    
    var sunrise: Double?
    var sunset: Double?
    
    var sunrise_str: String? {
        if let date = self.sunrise {
            let date_greg = Date(timeIntervalSince1970: date)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm"
            formatter.timeZone = .current
            let stringDate = formatter.string(from: date_greg)
            return stringDate
        } else {
            return nil
        }
    }
    
    var sunset_str: String? {
        if let date = self.sunset {
            let date_greg = Date(timeIntervalSince1970: date)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm"
            formatter.timeZone = .current
            let stringDate = formatter.string(from: date_greg)
            return stringDate
        } else {
            return nil
        }
    }
}
