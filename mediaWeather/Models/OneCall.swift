//
//  OneCall.swift
//  mediaWeather
//
//  Created by Pavel Belov on 13.10.2021.
//

import Foundation

class OneCall: Decodable {
   
    var current: CurrentData?
    var weather: [Weather]?
    var name: String?
    var visibility: Int?
    var daily: [Daily]?
    
}


class Weather: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    var iconUrl: String? {
        if let icon = self.icon {
            return "http://openweathermap.org/img/wn/\(icon)@4x.png"
        } else {
            return nil
        }
    }
}


class Current: Decodable {
    
    var sunrise: Double?
    var sunset: Double?
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var clouds: Int?
    var visibility: Int?
    var wind_speed: Int?
    var wind_deg: Int?
    
    var sunrise_str: String? {
        if let date = self.sunrise {
            let date_greg = Date(timeIntervalSince1970: date)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm"
            formatter.timeZone = .current
            let stringDate = formatter.string(from: date_greg)
            return stringDate
        } else {
            return nil
        }
    }
    
    var sunset_str: String? {
        if let date = self.sunset {
            let date_greg = Date(timeIntervalSince1970: date)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm"
            formatter.timeZone = .current
            let stringDate = formatter.string(from: date_greg)
            return stringDate
        } else {
            return nil
        }
    }
    
    var wind_segment: String {
        
        var seg: String = ""
        
        if let dir = self.wind_deg {
            switch dir {
            case 0..<90 :
                seg = "С-В"
            case 91..<180 :
                seg = "Ю-В"
            case 181..<270 :
                seg = "Ю-З"
            case 271..<360 :
                seg = "С-З"
            default:
                seg = ""
            }
        }
        
       return seg
    }
}

class Daily: Decodable {
    var dt: Int?
    var sunrise: Double?
    var sunset: Double?
    var temp: Temp?
    var feels_like: FeelsLike?
    var pressure: Int?
    var humidity: Int?
    var clouds: Int?
    var wind_speed: Int?
    var wind_deg: Int?
    var weather: [Weather]?
}

class Temp: Decodable {
    var day: Double?
}

class FeelsLike: Decodable {
    var day: Double?
}
    

