//
//  UrlPath.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlPath: String {
    case
    data = "/data/2.5/weather",
    onecall = "/data/2.5/onecall"
   
}
