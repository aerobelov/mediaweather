//
//  URLQueries.swift
//  oVK
//
//  Created by Pavel Belov on 23.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlQueries {
    
    case current(String),
         onecall(Double, Double)
    
    var apikey: String {
        return "c8bc20774399c735f5ce1df47a7b7849"
    }
    
    func fill() -> [URLQueryItem] {
        
        let units = URLQueryItem(name: "units", value: "metric")
        let lang = URLQueryItem(name: "lang", value: "ru")
    
        switch self {
        case .current(let city):
            return [URLQueryItem(name: "q", value: city),
                    URLQueryItem(name: "appid", value: apikey),
                    units,
                    lang]
        case .onecall(let lon, let lat):
            return [URLQueryItem(name: "lon", value: lon.str()),
                    URLQueryItem(name: "lat", value: lat.str()),
                    URLQueryItem(name: "exclude", value: "current,hourly,daily"),
                    URLQueryItem(name: "appid", value: apikey),
                    units,
                    lang]
            
        }
    }
    
}
