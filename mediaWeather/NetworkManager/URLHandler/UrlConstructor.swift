//
//  URLConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlConstructor  {
    
    case current, onecall
    
    func urls(_ query: [URLQueryItem] = []) -> Result<URL, UrlError> {
        switch self {
        case .current:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.http.rawValue
            urlComponent.host = UrlHost.weather.rawValue
            urlComponent.path = UrlPath.data.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
        case .onecall:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.http.rawValue
            urlComponent.host = UrlHost.weather.rawValue
            urlComponent.path = UrlPath.onecall.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
        }
    }
}
