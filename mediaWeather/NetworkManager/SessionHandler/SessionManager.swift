//
//  SessionManager.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

class SessionManager {
    
    typealias SessionHandler = Result<Data, SessionError>
    
    var url: URL?
    var request: URLRequest?
    
    var header: [String: String] = [:]
    var requestMethod: RequestMethod = .post
    var parameters: [URLQueryItem] = []
    var urlType: UrlConstructor = .current
    
    func downloadData (_ completion: @escaping (SessionHandler) -> ()) {
        
        let session = DefaultSession.returnSession()
        
        guard request != nil else { return completion(.failure(.SessionHandlerDomainError)) }
        
        session.dataTask(with: request!) { data, response, error in
            
            //print(response)
            guard let data = data, error == nil else {
                if let error = error as NSError?, error.domain == NSURLErrorDomain {
                    completion(.failure(.SessionHandlerDomainError))
                }
                return
            }
            
            guard let res = response as? HTTPURLResponse else {
                print("SessionHandler Server error")
                completion(.failure(.SessionHandlerstatusCodeError))
                return
            }
            
            switch res.statusCode {
            case 300...500:
                completion(.failure(.SessionHandlerstatusCodeError))
            default:
                print()
            }
            
            guard let mime = res.mimeType, mime == "application/json" else {
                completion(.failure(.SessionHendlermimeTypeError))
                return
            }
            
            do {
                let o = try JSONSerialization.jsonObject(with: data, options: [])
                //print("OBJECT=\(o)")
                completion(.success(data))
            } catch {
                completion(.failure(.SessionHandlerdecodingError))
            }
            
        }.resume()
    }
    
    func returnRequest() {
        if self.url != nil {
            self.request = RequestConstructor.ReturnBaseRequest(url!, method: requestMethod, header: header)
        }
    }
    
    func returnUrl() {
        let url = self.urlType.urls(parameters)
        switch url {
        case .success(let url):
            self.url = url
            //print("URL is \(url)")
        case .failure(let url):
            self.url = nil
            print("SessionManager is failed URL is \(url)")
        }
    }
    
    init(_ header: [String:String] = [:],
         _ requestMethod: RequestMethod,
         _ parameters: [URLQueryItem] = [],
         _ urlType: UrlConstructor
    ) {
        self.header = header
        self.requestMethod = requestMethod
        self.parameters = parameters
        self.urlType = urlType
        
        returnUrl()
        returnRequest()
    }
}
