//
//  DefaultSession.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

final class DefaultSession {
    
    static func returnSession () -> URLSession {
        
        let session = URLSession.init(configuration: .default)
        session.configuration.allowsCellularAccess = true
        session.configuration.timeoutIntervalForRequest = 3
        session.configuration.timeoutIntervalForResource = 3
        
        if #available(iOS 11.0, *) {
            session.configuration.waitsForConnectivity = false
        } else {
            
        }
        
        if #available(iOS 9.0, *) {
            session.configuration.shouldUseExtendedBackgroundIdleMode = false
        } else {
            
        }
        
        if #available(iOS 11.0, *) {
            session.configuration.multipathServiceType = .handover
        } else {
            
        }
        
        session.configuration.httpShouldUsePipelining = true
        
        if #available(iOS 13.0, *) {
            session.configuration.allowsExpensiveNetworkAccess = true
        } else {
            
        }
        
        if #available(iOS 13.0, *) {
            session.configuration.allowsConstrainedNetworkAccess = true
        }
        
        return session
    }
}
