//
//  SessionError.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

public enum SessionError: Error {
    case SessionHandlerDomainError
    case SessionHandlerstatusCodeError
    case SessionHendlermimeTypeError
    case SessionHandlerdecodingError
}
