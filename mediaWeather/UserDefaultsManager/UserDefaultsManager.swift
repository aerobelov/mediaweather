//
//  UserDefaultsManager.swift
//  mediaWeather
//
//  Created by Pavel Belov on 10.10.2021.
//

import Foundation

class UserDefaultsManager {
    
    let defaults = UserDefaults.standard
    
    func saveHome(city: String) {
        print("SAVING HOME")
        defaults.set(city, forKey: "Home")
        
    }
    
    func clear() {
        defaults.set(nil, forKey: "cities")
    }
    
    
    func add(city: String) {
        if var arr = defaults.stringArray(forKey: "cities") {
            if !arr.contains(city) {
                arr.append(city)
                defaults.set(arr, forKey: "cities")
            }
        } else {
            defaults.set([city], forKey: "cities")
        }
    }
    
    func get_home() -> String? {
        if let arr = defaults.stringArray(forKey: "cities"), let home = arr.first {
            return home
        } else {
            return nil
        }
    }
    
    func tryHome() -> Int? {
        if let array = defaults.stringArray(forKey: "cities") {
            return array.count
        } else {
            return nil
        }
    }
}
