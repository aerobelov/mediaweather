//
//  CoordinatorProtocols.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation
import UIKit

enum CoordinatorType {
    case app, search, tab, home, cities
}

protocol Coordinator: AnyObject {
    var finishDelegate: CoordinatorFinishDelegate? { get set }
    var navigationController: UINavigationController { get set }
    var childCoordinators: [Coordinator] { get set }
    var type: CoordinatorType { get set }
    init(_ navigationController: UINavigationController)
    func start()
    func finish()
}

extension Coordinator {
    func finish() {
        childCoordinators.removeAll()
        finishDelegate?.coordinatorDidFinish(childCoordinator: self)
    }
}

protocol CityProtocol {
    var homeCity: String? { get set }
}

protocol CoordinatorFinishDelegate {
    func coordinatorDidFinish(childCoordinator: Coordinator)
}

protocol AppCoordinatorProtocol {
    var storage: UserDefaultsManager { get set }
    func showSearchFlow()
    func showTabFlow()
}

protocol SearchCoordinatorProtocol: Coordinator {
    func showSearchCoordinator()
    var storage: UserDefaultsManager? { get set }
}

protocol TabBarCoordinatorProtocol: Coordinator {
    var tabBarController: UITabBarController { get set }
    func selectPage(_ page: TabBarPage)
    func setSelectedIndex(_ index: Int)
    func currentPage() -> TabBarPage?
}
