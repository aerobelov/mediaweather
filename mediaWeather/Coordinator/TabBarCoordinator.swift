//
//  TabBarCoordinator.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation
import UIKit



class TabBarCoordinator: NSObject, TabBarCoordinatorProtocol, UITabBarControllerDelegate {
    var tabBarController: UITabBarController
    
    var homeCity: String?
    
    var coords: (Double, Double)?
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator] = []
    
    var type: CoordinatorType = .tab
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.tabBarController = UITabBarController()
    }
    
    func start() {
        let pages: [TabBarPage] = [.home, .cities].sorted(by: { $0.pageOrder() < $1.pageOrder() } )
        let controllers: [UINavigationController] = pages.map({ getTabBarController($0)})
        prepareTabBarController(withTabControllers: controllers)
    }
    
    func selectPage(_ page: TabBarPage) {
        tabBarController.selectedIndex = page.pageOrder()
    }
    
    func setSelectedIndex(_ index: Int) {
        guard let page = TabBarPage(index: index) else { return }
        tabBarController.selectedIndex = page.pageOrder()
    }
    
    func currentPage() -> TabBarPage? {
        return TabBarPage(index: tabBarController.selectedIndex)
    }
    
    func prepareTabBarController(withTabControllers controllers: [UIViewController]) {
        tabBarController.delegate = self
        tabBarController.setViewControllers(controllers, animated: true)
        tabBarController.selectedIndex = TabBarPage.home.pageOrder()
        //tabBarController.tabBar.isTranslucent = false
        navigationController.viewControllers = [tabBarController]
    }
    
    private func getTabBarController(_ page: TabBarPage) -> UINavigationController {
        let navController = UINavigationController()
        navController.setNavigationBarHidden(true, animated: false)
        navController.tabBarItem = UITabBarItem(title: page.pageTitle(), image: UIImage(systemName: page.image()), tag: page.pageOrder())
        switch page {
        case .home:
            let homeVC = HomeViewController(city: self.homeCity!)
            homeVC.didSendEventClosure = { [weak self] event in
                switch event {
                case .daily:
                    self?.selectPage(.cities)
                default:
                    print("default")
                }
            }
            navController.pushViewController(homeVC, animated: true)
        case .cities:
            let citiesVC = CitiesViewController()
            citiesVC.didSendEventClosure = { [weak self] event in
                switch event {
                case .detail:
                    print("DETAIL")
                }
            }
            navController.pushViewController(citiesVC, animated: true)
        }
        return navController
    }
    
}
