//
//  TypeExtensions.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation

extension Double {
    func str()-> String {
        return String(self)
    }
}

extension Int {
    func str()-> String {
        return String(self)
    }
}
