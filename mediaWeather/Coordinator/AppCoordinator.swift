//
//  AppCoordinator.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation
import UIKit

//APPCOORDINATOR

class AppCoordinator: Coordinator, CoordinatorFinishDelegate, CityProtocol {
    
    var homeCity: String?
    
    var coords: (Double, Double)?
    
    var finishDelegate: CoordinatorFinishDelegate? = nil
    
    var navigationController: UINavigationController
    
    var childCoordinators = [Coordinator]()
    
    var type: CoordinatorType = .app
    
    var storage: UserDefaultsManager
    
    
    required init(_ navigationController: UINavigationController) {
        self.storage = UserDefaultsManager()
        self.navigationController = navigationController
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    func start() {
        storage.clear()
        if let city = storage.get_home()  {
            print("SHOWING TABS")
            self.homeCity = city
            showTabsFlow(city)
        } else {
            print("SHOWING SEARCH")
            showSearchFlow()
        }
        
    }
    
    func showTabsFlow(_ city: String) {
        let tabCoordinator = TabBarCoordinator(navigationController)
        tabCoordinator.finishDelegate = self
        tabCoordinator.homeCity = city
        tabCoordinator.start()
        childCoordinators.append(tabCoordinator)
    }
    
    func showSearchFlow() {
        let searchCoordinator = SearchCoordinator(navigationController)
        searchCoordinator.finishDelegate = self
        searchCoordinator.cityDelegate = self
        searchCoordinator.storage = storage
        searchCoordinator.start()
        childCoordinators.append(searchCoordinator)
    }
    
}

extension AppCoordinator {
    func coordinatorDidFinish(childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter( { $0.type != childCoordinator.type} )
        switch childCoordinator.type {
        case.search:
            childCoordinators.removeAll()
            if let home = self.homeCity {
                showTabsFlow(home)
            }
        case .home:
            print("")
        case .cities:
            print("")
        default:
            print("")
        }
    }
}
