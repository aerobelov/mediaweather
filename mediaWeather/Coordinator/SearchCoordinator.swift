//
//  SearchCoordinator.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation
import UIKit

class SearchCoordinator: SearchCoordinatorProtocol {
  
    var finishDelegate: CoordinatorFinishDelegate?
    
    var cityDelegate: CityProtocol?
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator] = []
    
    var type: CoordinatorType = .search
    
    weak var storage: UserDefaultsManager?
    
   
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        print("SEARCH COORD START")
        showSearchCoordinator()
    }
    
    func showSearchCoordinator() {
        let searchVC = SearchViewController()
        searchVC.didSendEventClosure = { [weak self] event in
            switch event {
                case .found(let city):
                self?.cityDelegate?.homeCity = city
                self?.storage?.add(city: city)
                self?.finishDelegate?.coordinatorDidFinish(childCoordinator: self!)
            }
            
        }
        self.navigationController.pushViewController(searchVC, animated: true)
    }
    
    
}
