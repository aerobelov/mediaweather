//
//  ViewController.swift
//  mediaWeather
//
//  Created by Pavel Belov on 19.10.2021.
//

import UIKit

class CitiesViewController: UIViewController {
    
    var didSendEventClosure: ((CitiesViewController.Event)->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    init() {
        super.init(nibName: "CitiesViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CitiesViewController {
    enum Event {
        case detail
    }
}
