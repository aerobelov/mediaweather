//
//  TableViewCell.swift
//  mediaWeather
//
//  Created by Pavel Belov on 24.10.2021.
//

import UIKit

class DailyTableViewCell: UITableViewCell{
    
    @IBOutlet weak var dt: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var like: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var icon: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(dailyCellModel: Daily) {
        self.dt.text = dailyCellModel.dt?.str()
        self.temp.text = dailyCellModel.temp?.day?.str()
        self.like.text = dailyCellModel.feels_like?.day?.str()
        self.pressure.text = dailyCellModel.pressure?.str()
        self.humidity.text = dailyCellModel.humidity?.str()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
