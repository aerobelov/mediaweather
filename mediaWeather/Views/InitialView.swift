//
//  InitialView.swift
//  mediaWeather
//
//  Created by Pavel Belov on 07.10.2021.
//

import UIKit

class InitialView: UIView {
    var label: UILabel!
    var searchField: UITextField!
    var initialView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        searchField = UITextField(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        label.text = "Введите город"
    }
    
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented.")
    }

}
