//
//  HomeViewController.swift
//  mediaWeather
//
//  Created by Pavel Belov on 19.10.2021.
//

import UIKit

class HomeViewController: UIViewController
{
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temp_now: UILabel!
    @IBOutlet weak var temp_like: UILabel!
    @IBOutlet weak var descr: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var pressureIcon: UIImageView!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var minmax: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var visIcon: UIImageView!
    @IBOutlet weak var visibility: UILabel!
    @IBOutlet weak var sunrise: UILabel!
    @IBOutlet weak var sunriseIcon: UIImageView!
    @IBOutlet weak var sunset: UILabel!
    @IBOutlet weak var sunsetIcon: UIImageView!
    @IBOutlet weak var humidityIcon: UIImageView!
    @IBOutlet weak var maxIcon: UIImageView!
    @IBOutlet weak var windIcon: UIImageView!
    @IBOutlet weak var dailyButton: UIButton!
    
    var initial: String!
    var model: HomeViewModel!
    var coords: (Double, Double)?
    
    var didSendEventClosure: ((HomeViewController.Event)-> Void)?
    
    
    init(city: String) {
        super.init(nibName: "HomeViewController", bundle: nil)
        model = HomeViewModel()
        model.initial_name = city
        model.transferCoords = { [weak self] (lon, lat) in
            self?.coords = (lon, lat)
        }
        model.retrive()
        model.onUpdate = { [unowned self] in
            self.configure()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
//    @IBAction func onDetailPressed() {
//        self.coordinator?.dailyFlow()
//    }
    
    func configure()->Void {
        
        DispatchQueue.main.async {
            
            self.city.text = self.model.current?.name
            self.temp_now.text = self.model.current?.main?.temp?.str()
            self.descr.text = self.model.current?.weather?.first?.description
            self.temp_like.text = "Ощущается как \(self.model.current?.main?.feels_like?.str() ?? "")"
            if let urlString = self.model.current?.weather?.first?.iconUrl {
                let url = URL(string: urlString)
                self.icon.load(url: url!)
            }
            self.pressureIcon.image = UIImage(systemName: "rectangle.compress.vertical")
            if let press = self.model.current?.main?.pressure?.str() {
                self.pressure.text = "\(press) мБ"
            }
            
            self.humidityIcon.image = UIImage(systemName: "humidity")
            if let hum = self.model.current?.main?.humidity?.str() {
                self.humidity.text = "\(hum) %"
            }
            
            self.maxIcon.image = UIImage(systemName: "thermometer")
            if let min = self.model.current?.main?.temp_min?.str(), let max = self.model.current?.main?.temp_max?.str() {
                self.minmax.text = "Мин.\(min) C, Mакс.\(max) C"
            }
            
            self.windIcon.image = UIImage(systemName: "wind")
            if let deg = self.model.current?.wind?.wind_segment, let speed = self.model.current?.wind?.speed?.str() {
                var gust_str = ""
                if let gust = self.model.current?.wind?.gust?.str() {
                    gust_str = ", порывы \(gust) м/с"
                }
                self.wind.text = "Ветер \(deg), \(speed) м/с\(gust_str)"
            }
            
            self.visIcon.image = UIImage(systemName: "moon")
            if let vis = self.model.current?.visibility?.str() {
                self.visibility.text = "Видимость \(vis) м."
            }
            
            self.sunriseIcon.image = UIImage(systemName: "sunrise")
            if let rise = self.model.current?.sys?.sunrise_str {
                self.sunrise.text = "Восход \(rise)"
            }
            
            self.sunsetIcon.image = UIImage(systemName: "sunset")
            if let set = self.model.current?.sys?.sunset_str {
                self.sunset.text = "Заход \(set)"
            }
        }
        
    }
    
   
    func getCurrentWeather() {
        self.model.retrive()
    }
    
//    func getDaily() {
//        if let coords = self.model.current?.coord {
//            self.model.getDaily()
//        }
//    }
   
}

extension HomeViewController {
    enum Event {
        case daily
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
