//
//  TabBarFlow.swift
//  mediaWeather
//
//  Created by Pavel Belov on 22.04.2022.
//

import Foundation

enum TabBarPage {
    case home
    case cities
    
    init?(index: Int) {
        switch index {
            case 0: self = .home
            case 1: self = .cities
            default: self = .home
        }
    }
    
    func pageTitle() -> String {
        switch self {
        case .home:
            return "Home"
        case .cities:
            return "Cities"
        }
    }
    
    func pageOrder() -> Int {
        switch self {
        case .home:
            return 0
        case .cities:
            return 1
        }
    }
    
    func image() -> String {
        switch self {
        case .home:
            return "house"
        case .cities:
            return "list.dash"
        }
    }
}
