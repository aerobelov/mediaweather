//
//  SearchTableViewCell.swift
//  mediaWeather
//
//  Created by Pavel Belov on 13.10.2021.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var temp: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
