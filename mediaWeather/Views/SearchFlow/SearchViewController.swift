//
//  InitialViewController.swift
//  mediaWeather
//
//  Created by Pavel Belov on 07.10.2021.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //var parentCoordinator: SearchBaseCoordinator?
    
   
    var label: UILabel!
    var searchLine: UITextField?
    var initialView: UIView!
    var didSendEventClosure: ((SearchViewController.Event)->Void)?
    var currents: [CurrentData] = []
   
    
    private var myTableView: UITableView!
    
    
    init() {
        print("SEARCH CONTRO INIT")
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currents.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let name = currents[indexPath.row].name {
            print("IS NAME \(name)")
            didSendEventClosure?(Event.found(city: name))
        }
        //currents[indexPath.row].name)  //onCitySelected?(currents[indexPath.row].name!)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath) as! SearchTableViewCell
        if self.currents.count > 0 {
            cell.name.text = currents[indexPath.row].name
            cell.temp.text = currents[indexPath.row].main?.temp?.str()
        }
        return cell
    }
    
    
    override func loadView() {
        super.loadView()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.configure()
    }
    
    @objc func search() {
        let arg = self.searchLine?.text ?? ""
        let queries = UrlQueries.current(arg).fill()
        let session = SessionManager([:], .post, queries, .current)
        session.downloadData { result in
            switch result {
            case .success(let data):
                if let reply = try? JSONDecoder().decode(CurrentData.self, from: data) {
                    self.currents.append(reply)
                    DispatchQueue.main.async {
                        self.myTableView.reloadData()
                    }
                    //print(reply.main?.temp)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func configure() {
        print("SEARCH CONF")
        
        self.searchLine = UITextField(frame: CGRect(x: 20, y: 100, width: 300, height: 80))
        searchLine!.placeholder = "Введите город"
        searchLine!.backgroundColor = UIColor.white
        searchLine!.layer.cornerRadius = 10
        
        self.view.addSubview(searchLine!)
        searchLine!.translatesAutoresizingMaskIntoConstraints = false
        
        let goButton = UIButton(type:.system)
        goButton.setTitle("Поиск", for: .normal)
        goButton.addTarget(self, action: #selector(search), for: .touchUpInside)
        goButton.backgroundColor = UIColor.blue
        goButton.tintColor = .white
        goButton.layer.cornerRadius = 10
        self.view.addSubview(goButton)
        goButton.translatesAutoresizingMaskIntoConstraints = false
        
        let barHeight: CGFloat = self.searchLine?.frame.height ?? 100
        let displayWidth: CGFloat = self.view.frame.width
        
        myTableView = UITableView(frame: CGRect(x: 0, y: 100, width: view.frame.width, height: view.frame.height), style: .plain)
        let nibcell = UINib(nibName: "SearchTableViewCell", bundle: nil)
        
        myTableView.register(nibcell, forCellReuseIdentifier: "MyCell")
        
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
        
        
        
        
        NSLayoutConstraint.activate([
            searchLine!.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            searchLine!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            searchLine!.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6),
            searchLine!.heightAnchor.constraint(equalToConstant: 40),
            
            goButton.topAnchor.constraint(equalTo: searchLine!.topAnchor),
            goButton.heightAnchor.constraint(equalTo: searchLine!.heightAnchor),
            goButton.leadingAnchor.constraint(equalTo: searchLine!.trailingAnchor, constant: 5),
            goButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            myTableView.topAnchor.constraint(equalTo: searchLine!.bottomAnchor, constant: 10),
            myTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            myTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            myTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
    }

}

extension SearchViewController {
    enum Event {
        case found(city: String)
    }
}
